from typing import Optional

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import json
import random

from flask import jsonify

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def read_root():
    return {"Hello": "Bienvenue sur le serveur de Quentin :)"}

@app.get("/randomlist")
def random_list():
    numberOfPlayers = random.randint(4,8)
    pseudos = []
    for i in range(numberOfPlayers):
        pseudo = "j" + str(i)
        pseudos.append(pseudo)
    return pseudos
